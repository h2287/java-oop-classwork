package sample;

import java.util.HashMap;
import java.util.Map;

public class Main {

	public static void main(String[] args) {

		Map<String, String> map = new HashMap<>();
		map.put("h", "Hello");
		map.put("j", "Java");
		map.put("w", "World");

		System.out.println(map);

		String value = map.get("hh");
		System.out.println(value);

		map.put("j", "JS");
		System.out.println(map);
		
		for (String k : map.keySet()) {
			System.out.println(k + " " + map.get(k));
		}

	}

}
