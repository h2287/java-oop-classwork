package sample;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.Map;

public class NetworkService {

	public static String getStringFromURL(String spec, String code) throws IOException {
		URL url = new URL(spec);
		URLConnection conn = url.openConnection();
		try (BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), code))) {
			String result = "";
			String temp = "";
			for (;;) {
				temp = br.readLine();
				if (temp == null) {
					break;
				}
				result += temp + System.lineSeparator();
			}
			return result;
		}
	}

	public static long getFileFromURL(String spec, File folder) throws IOException {
		URL url = new URL(spec);
		URLConnection conn = url.openConnection();
		int n = spec.lastIndexOf("/");
		String fileName = spec.substring(n + 1);
		try (InputStream is = conn.getInputStream();
				OutputStream os = new FileOutputStream(new File(folder, fileName))) {
			return is.transferTo(os);
		}
	}

	public static Map<String, List<String>> getHeadersFromURL(String spec) throws IOException {
		URL url = new URL(spec);
		URLConnection conn = url.openConnection();
		return conn.getHeaderFields();
	}
}