package sample;

import java.util.Arrays;
import java.util.Comparator;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Cat cat1 = new Cat("Luska", 8);
		Cat cat2 = new Cat("Barsick", 4);
		Cat cat3 = new Cat("Umka", 12);
		Cat cat4 = new Cat("Markiz", 6);
		Cat cat5 = new Cat("Murka", 3);

		Cat[] cats = new Cat[] { cat1, cat2, cat3, cat4, cat5 };
		for (Cat cat : cats) {
			System.out.println(cat);
		}
		System.out.println();

		Arrays.sort(cats, Comparator.nullsFirst(new CatAgeComparator()));

		for (Cat cat : cats) {
			System.out.println(cat);
		}

	}

}