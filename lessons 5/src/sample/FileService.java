package sample;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class FileService {

	public static long copyFile(File fileIn, File fileOut) throws IOException {
		try (InputStream is = new FileInputStream(fileIn); OutputStream os = new FileOutputStream(fileOut)) {
			return is.transferTo(os);
		}
	}

	public static long copyAllFiles(File folderIn, File folderOut) throws IOException {
		File[] files = folderIn.listFiles();
		long totalByte = 0;
		for (File file : files) {
			if (file.isFile()) {
				File fileOut = new File(folderOut, file.getName());
				totalByte += copyFile(file, fileOut);
			}
		}
		return totalByte;
	}

}
