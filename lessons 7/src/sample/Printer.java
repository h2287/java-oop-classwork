package sample;

public class Printer {

	private long threadId = 1;

	public synchronized void printText(String text) {

		long thrId = Thread.currentThread().getId();

		for (; threadId == thrId;) {
			try {
				wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.print(text + " ");
		threadId = thrId;
		notifyAll();
	}
}