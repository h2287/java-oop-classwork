package sample;

import java.lang.reflect.Field;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Cat cat1 = new Cat("Luska", 8);
		Cat cat2 = new Cat("Luska", 8);
		Cat cat3 = new Cat("Luska", 8);

		System.out.println(cat1);
		System.out.println(cat2);
		System.out.println("=====================================");
		System.out.println(cat1 == cat2);
		System.out.println(cat1.equals(cat2));
		System.out.println("=====================================");
		System.out.println("cat1 eq cat1 = " + cat1.equals(cat1));
		System.out.println("cat1 eq cat2 = " + cat1.equals(cat2) + ", cat2 eq cat1 = " + cat2.equals(cat1));
		System.out.println("cat1 eq cat2 = " + cat1.equals(cat2) + ", cat2 eq cat3 = " + cat2.equals(cat3)
				+ " => cat1 eq cat3 = " + cat1.equals(cat3));
		System.out.println("cat1 eq cat2 = " + cat1.equals(cat2) + " cat1.hashCode() = " + cat1.hashCode()
				+ ", cat2.hashCode() = " + cat2.hashCode());
		System.out.println("=====================================");

		Cat cat4 = null;

		try {
			cat4 = cat1.clone();
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println(cat4 != cat1);
		System.out.println(cat4.getClass().equals(cat1.getClass()));
		System.out.println(cat4.equals(cat1));
		System.out.println("=====================================");

		Class catClass = Cat.class; /// cat1.getClass();
		Field[] catFields = catClass.getDeclaredFields();
		for (Field field : catFields) {
			System.out.println(field);
		}
		System.out.println("=====================================");

		Class[] interfaces = catClass.getInterfaces();
		for (Class class1 : interfaces) {
			System.out.println(class1);
		}

		try {
			Field catAge = catClass.getDeclaredField("age");
			catAge.setAccessible(true);
			catAge.setInt(cat1, 10);
		} catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println(cat1);

	}
}