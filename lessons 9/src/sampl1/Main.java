package sampl1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		List<String> myList = new ArrayList<>();

		myList.add("Java");
		myList.add("the");
		myList.add("best");

		System.out.println(myList);

		myList.add(1, " JS");

		System.out.println(myList);

		String text = myList.get(1);
		System.out.println(text);

		myList.remove(1);

		System.out.println(myList);

		Collections.sort(myList);

		System.out.println(myList);

		for (String element : myList) {
			System.out.println(element);
		}
		System.out.println();

		Iterator<String> itr = myList.iterator();

		for (; itr.hasNext();) {
			String element = itr.next();
			System.out.println(element);
		}

	}

}
