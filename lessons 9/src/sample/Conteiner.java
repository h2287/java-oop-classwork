package sample;

public class Conteiner<E> {

	private Object element;

	public Conteiner(E element) {
		super();
		this.element = element;
	}

	public E getElement() {
		return(E) element;
	}

	public void setElement(Object element) {
		this.element = element;
	}

	@Override
	public String toString() {
		return "Conteiner [element= " + element + "]";
	}

}
