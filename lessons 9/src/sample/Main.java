package sample;

import java.awt.Container;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Conteiner<Integer> a = new Conteiner<>(100);

//    a.setElement("Java");

		int sum = 50 + a.getElement();

		System.out.println("Sum = " + sum);

		Conteiner<String> b = new Conteiner<>("Hello world");

		System.out.println(a);
		System.out.println(b);

		Integer[] arr1 = new Integer[] { -2, -5, -1 };

		Integer max1 = getMax(arr1);

		System.out.println(max1);

		
	}

	public static <T extends Comparable<T>> T getMax(T[] array) {
		T max = array[0];
		for (T element : array) {
			if (element.compareTo(max) > 0) {
				max = element;
			}
		}
		return max;
	}

}